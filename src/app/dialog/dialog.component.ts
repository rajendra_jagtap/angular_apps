import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  constructor(
    public dialog: MatDialog,
    private _studentService: StudentService,
    private snackbar: MatSnackBar
    ) { }

  form: FormGroup = new FormGroup({
    name: new FormControl(''),
    age: new FormControl(''),
    gendor: new FormControl('1'),
    city: new FormControl(''),
    email: new FormControl(''),
  });

  updateData() {
    const customObj = {
      id: this._studentService.studentList.length,
      name: this.form.value.name,
      age: this.form.value.age,
      gendor: this.form.value.gendor,
      city: this.form.value.city,
      email: this.form.value.email
    }
    this._studentService.studentList.push(customObj);
    this.snackbar.open('Student Added Successfully !', null, {
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  resetForm() {
    this.form.reset();
  }
}
