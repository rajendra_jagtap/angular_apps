import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { DialogComponent } from './dialog/dialog.component';
import { StudentService } from './student.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public grid: boolean = true;
  public studentList = this._studentService.getStudents();
  public filteredStudents = this.studentList.slice(0,5);
  private _searchTerm: string;

  constructor(public dialog: MatDialog, private _studentService: StudentService) {}
 
  get searchTerm(): string {
    return this._searchTerm;
  }

  set searchTerm(value: string) {
    if (value.length == 0) {
      this.filteredStudents = this.studentList.slice(0,5);
    } else {
      this._searchTerm = value;
      this.filteredStudents = this.updateStudentList(value);
    }
  }

  updateStudentList(searchString: string) {
    return this.studentList.filter(student => student.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1);
  }

  onPageChangeEvent(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.studentList.length) {
      endIndex = this.studentList.length;
    }
    this.filteredStudents = this.studentList.slice(startIndex, endIndex);
  }

  toggleView() {
    this.grid = !this.grid;
  }

  deleteRecord(studentId: number) {
    if (studentId) {
      const index = this.studentList.findIndex(x => x.id === studentId);
      this.studentList.splice(index, 1);
      this.filteredStudents = this.studentList.slice(0,5);
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '270px',
      height: '414px',
      data: this.studentList
    });
  }
}
