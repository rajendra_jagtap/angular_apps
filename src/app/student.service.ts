import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }
  
  public studentList = [
    {
      id: 1,
      name: 'John Doe',
      age: 46,
      gendor: 'Male',
      city: 'Bihar',
      email: 'john.doe@email.com',
    },
    {
      id: 2,
      name: 'Hydra Dynamo',
      age: 25,
      gendor: 'Male',
      city: 'Mumbai',
      email: 'hydra.dynamo@email.com',
    },
    {
      id: 3,
      name: 'Soul Mortal',
      age: 22,
      gendor: 'Male',
      city: 'Gujrat',
      email: 'soul.mortal@email.com',
    },
    {
      id: 4,
      name: 'Emiway Bantai',
      age: 26,
      gendor: 'Male',
      city: 'Punjab',
      email: 'honey.singh@email.com',
    },
    {
      id: 5,
      name: 'Kishor Kumar',
      age: 77,
      gendor: 'Male',
      city: 'London',
      email: 'kishor.kumar@email.com',
    },
    {
      id: 6,
      name: 'Brad Pitt',
      age: 43,
      gendor: 'Male',
      city: 'Sweden',
      email: 'brad.pitt@email.com',
    },
    {
      id: 7,
      name: 'Scarlett Johnson',
      age: 29,
      gendor: 'Female',
      city: 'New York',
      email: 'scar.johnson@email.com',
    },
    {
      id: 8,
      name: 'Jason Statham',
      age: 45,
      gendor: 'Male',
      city: 'Beijing',
      email: 'jason.statham@email.com',
    },
    {
      id: 9,
      name: 'Harry Potter',
      age: 15,
      gendor: 'Male',
      city: 'Shanghai',
      email: 'harry.potter@email.com',
    },
    {
      id: 10,
      name: 'Linda Loman',
      age: 39,
      gendor: 'Female',
      city: 'Jammu',
      email: 'linda.loman@email.com',
    },
    {
      id: 11,
      name: 'Little Nora',
      age: 20,
      gendor: 'Female',
      city: 'Kashmir',
      email: 'little.nora@email.com',
    },
    {
      id: 12,
      name: 'Bob Marley',
      age: 55,
      gendor: 'Male',
      city: 'Paris',
      email: 'bob.marly@email.com',
    },
    {
      id: 12,
      name: 'Vashti Hoke',
      age: 34,
      gendor: 'Male',
      city: 'Varanasi',
      email: 'vashti.hoke@email.com',
    },
    {
      id: 13,
      name: 'Russ Stickly',
      age: 27,
      gendor: 'Male',
      city: 'Pune',
      email: 'russ.stickly@email.com',
    },
    {
      id: 14,
      name: 'Lucina Grove',
      age: 30,
      gendor: 'Female',
      city: 'Sydney',
      email: 'lucina.grove@email.com',
    },
    {
      id: 15,
      name: 'Avery Lamborn',
      age: 25,
      gendor: 'Female',
      city: 'Lahore',
      email: 'avery.lamborn@email.com',
    }
  ];

  getStudents() {
    return this.studentList;
  }
}
